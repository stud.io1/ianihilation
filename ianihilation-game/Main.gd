extends Node

onready var LOCAL_GAME = preload("Games/Game.tscn")

func _on_Main_menu_start_local_game():
	var local =  LOCAL_GAME.instance()
	get_node("/root/Main").add_child(local)
	get_node("Main_menu").queue_free()
