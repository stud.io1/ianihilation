extends HBoxContainer

var scores = {"blue": 0, "red": 0, "green": 0, "purple": 0, "orange": 0, "turquoise": 0}

func _ready():
	get_node("blue/Number").text = str(scores["blue"])
	get_node("red/Number").text = str(scores["red"])
	get_node("green/Number").text = str(scores["green"])
	get_node("purple/Number").text = str(scores["purple"])
	get_node("orange/Number").text = str(scores["orange"])
	get_node("turquoise/Number").text = str(scores["turquoise"])

func _on_Control_end_round(winner):
	scores[winner] += 1
	get_node(winner + "/Number").text = str(scores[winner])
