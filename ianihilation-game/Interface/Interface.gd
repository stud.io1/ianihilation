extends Control

signal health_changed(health)
signal ammu_counter_changed(bullet_stock)
signal end_round(winner)

func _on_map_ammu_counter_changed(bullet_stock):
	emit_signal("ammu_counter_changed", bullet_stock)

func _on_map_health_changed(health):
	emit_signal("health_changed", health)

func _on_map_end_round(winner):
	emit_signal("end_round", winner)

var number_of_players
var colors

func _ready():
	number_of_players = get_parent().number_of_players
	colors = get_parent().colors
	for j in colors.slice(number_of_players, colors.size() - 1, 1, true):
		get_node("score/" + j).hide()

