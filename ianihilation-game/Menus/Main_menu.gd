extends Node

signal start_local_game()

func _on_local_game_start_local_game():
	emit_signal("start_local_game")
