extends Button

signal start_local_game()

func _ready():
	self.connect("pressed", self, "_button_pressed")

func _button_pressed():
	emit_signal("start_local_game")
