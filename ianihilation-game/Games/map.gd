extends Node

signal health_changed(health)
signal ammu_counter_changed(bullet_stock)
signal tank_dead(player_color)
signal end_round(winner)

func _on_Tank_health_changed(health):
	emit_signal("health_changed", health)

func _on_Tank_tank_dead(player_color):
	emit_signal("tank_dead", player_color)
	alive_players.erase(player_color)
	if alive_players.size() == 1:
		var winner = alive_players[0]
		emit_signal("end_round", winner)

func _on_Tank_ammu_counter_changed(bullet_stock):
	emit_signal("ammu_counter_changed", bullet_stock)

var number_of_players
var alive_players = []
var my_color
var colors
var number_of_rows = 8
var number_of_columns = 15
# we start as if we came from the left to the top left point
var walls_to_destroy = []
var walls = init_walls()
var M = init_matrix()
var M0 = init_ref_matrix()
var number_walls_to_destroy = 6

func _ready():
	# get game info
	number_of_players = get_parent().number_of_players
	my_color = get_parent().my_color
	colors = get_parent().colors
	
	# create map
	while M != M0:
		var wall = select_random_valid_wall()
		walls.erase(wall)
		walls_to_destroy.push_back(wall)
		update_matrix(wall)
	destroy_some_more_walls(number_walls_to_destroy)
	destroy_walls()
	place_tanks()

func place_tanks():
	var used_spawn = [[0,0]]
	for i in colors.slice(0, number_of_players - 1, 1, true):
		var row = 0
		var col = 0
		while used_spawn.has([row,col]):
			var rng = RandomNumberGenerator.new()
			rng.randomize()
			row = rng.randi_range(1, number_of_rows)
			rng.randomize()
			col = rng.randi_range(1, number_of_columns)
		used_spawn.push_back([row,col])
		var center = "centers/row_" + str(row) + "/" + str(col)
		get_node("players/Tank_" + i).global_position = get_node(center).global_position
		alive_players.push_back(i)
		if i == my_color:
			get_node("players/Tank_" + i).controlled = true
	for j in colors.slice(number_of_players, colors.size() - 1, 1, true):
		get_node("players/Tank_" + j).queue_free()

func destroy_some_more_walls(num):
	for _i in range(num):
		var wall = null
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var random_number = rng.randi_range(0, walls.size() - 1)
		wall = walls[random_number]
		walls.erase(wall)
		walls_to_destroy.push_back(wall)

func init_matrix():
	var Mat = []
	for i in range(number_of_rows):
		Mat.push_back([])
		for j in range(number_of_columns):
			Mat[i].push_back(i*number_of_columns + j)
	return Mat
	
func init_ref_matrix():
	var Mat = []
	for i in range(number_of_rows):
		Mat.push_back([])
		for _j in range(number_of_columns):
			Mat[i].push_back(0)
	return Mat

func init_walls():
	var arr = []
	for i in range(number_of_rows):
		for j in range(number_of_columns):
			if i < number_of_rows - 1:
				arr.push_back([i,j,"bottom"])
			if j < number_of_columns - 1:
				arr.push_back([i,j,"right"])
	return arr

func destroy_walls():
	for i in walls_to_destroy:
		# translate matrix indices (start at 0) to node names (start at 1)
		var row = str(i[0]+1)
		var col = str(i[1]+1)
		$walls.get_node("row_"+ row).get_node(i[2]).get_node(col).queue_free()
	
func update_matrix(wall):
	var row = wall[0]
	var col = wall[1]
	var dir = wall[2]
	var val1 = null
	var val2 = null
	if dir == "right":
		val1 = M[row][col]
		val2 = M[row][col + 1]
	else :
		val1 = M[row][col]
		val2 = M[row + 1][col]
	var val_min = min(val1,val2)
	var val_max = max(val1,val2)
	for i in range(number_of_rows):
		for j in range(number_of_columns):
			if M[i][j] == val_max:
				M[i][j] = val_min

func select_random_valid_wall():
	var done = 0
	var wall = null
	while done == 0:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		var random_number = rng.randi_range(0, walls.size() - 1)
		wall = walls[random_number]
		var row = wall[0]
		var col = wall[1]
		var dir = wall[2]
		var val1 = null
		var val2 = null
		if dir == "right":
			val1 = M[row][col]
			val2 = M[row][col + 1]
		else :
			val1 = M[row][col]
			val2 = M[row + 1][col]
		if val1 == val2:
			walls.erase(wall)
		else:
			done = 1
	return wall


