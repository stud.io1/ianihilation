extends Node

signal end_game()

func _on_map_tank_dead(player_color):
	$tank_destroyed.play()

func _on_map_end_round(winner):
	$end_round.get_child(0).text = winner + " wins"
	$end_round.popup()
	# To do
	# timer
	# reload map
	# hide pop up

export (int) var number_of_players = 2
export (String,
	"blue",
	"red", 
	"green",
	"purple", 
	"orange",
	"turquoise"
	) var my_color = "blue"

var colors = [
	"blue",
	"red", 
	"green",
	"purple", 
	"orange",
	"turquoise"
	]
