extends KinematicBody2D

var velocity = Vector2()
var speed = 400
var life_time = 2.5
var shooter = null

func _ready():
	$shot.play()

func _physics_process(delta):
	var collision = move_and_collide(velocity * speed * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		if collision.collider.has_method("hit"):
			collision.collider.hit(20)
			self.die()
	life_time -= delta
	if life_time < 0:
		self.die()

func die():
	if shooter:
		shooter.reload()
	self.queue_free()
