extends KinematicBody2D

signal health_changed(health)
signal ammu_counter_changed(bullet_stock)
signal tank_dead(player_color)

export (int) var speed = 200
export (float) var rotation_speed = 2.5
export (int) var max_health = 100
export (int) var max_bullet_stock = 5
export (String,
	"blue",
	"red", 
	"green",
	"purple", 
	"orange",
	"turquoise"
	) var player_color
export (bool) var controlled = false

var velocity = Vector2()
var rotation_dir = 0
var health = 1
var bullet_stock = 0

onready var BULLET = preload("bullet.tscn")
var sprite = {
	"blue": preload("res://Assets/tank/tank_blue.png"),
	"red": preload("res://Assets/tank/tank_red.png"),
	"green": preload("res://Assets/tank/tank_green.png"),
	"purple": preload("res://Assets/tank/tank_purple.png"),
	"orange": preload("res://Assets/tank/tank_orange.png"),
	"turquoise": preload("res://Assets/tank/tank_turquoise.png"),
	}

func _ready():
	health = max_health
	bullet_stock = max_bullet_stock
	emit_signal("health_changed", health)
	emit_signal("ammu_counter_changed", bullet_stock)
	get_node("tank_sprite").texture = sprite[player_color]

func get_input():
	rotation_dir = 0
	velocity = Vector2()
	if Input.is_action_pressed('ui_right'):
		rotation_dir += 1
	if Input.is_action_pressed('ui_left'):
		rotation_dir -= 1
	if Input.is_action_pressed('ui_down'):
		velocity = Vector2(-speed, 0).rotated(rotation)
	if Input.is_action_pressed('ui_up'):
		velocity = Vector2(speed, 0).rotated(rotation)
	if Input.is_action_just_pressed("ui_select") && bullet_stock:
		self.shoot(BULLET)

func _physics_process(delta):
	if controlled :
		get_input()
	rotation += rotation_dir * rotation_speed * delta
	velocity = move_and_slide(velocity)
	if health < 1:
		self.die()

func hit(degat):
	health -= degat
	health = max(0, health)
	if controlled :
		emit_signal("health_changed", health)

func shoot(bullet_type):
	var bullet =  bullet_type.instance()
	get_parent().add_child(bullet)
	bullet.global_position = $shot_spawn.global_position
	bullet.velocity = Vector2(1, 0).rotated(rotation)
	bullet.shooter = self
	bullet_stock -= 1
	if controlled :
		emit_signal("ammu_counter_changed", bullet_stock)
	
func reload():
	bullet_stock += 1
	if controlled :
		emit_signal("ammu_counter_changed", bullet_stock)

func die():
	self.queue_free()
	emit_signal("tank_dead", player_color)
